# Assignment 1

## Domain
We have modelled Finn.no listings. We have considered Listing as an abstract class with subclasses such as BikeListing. Each listing is connected to a user and a user can be connected to multiple listings.

A listing contains an id, title, description, creation date, price, and coordinates (location). For bike listings there is also bike type and condition.

A user has email, name, and phone number.

![Finn image](finn.png "Finn listing page")

## Model
The ecore model is shown below. The enums attributes for bikeListing are defined separetely.

![model image](model.png "Ecore model")

## Instances
Instances can be found in the main class where we created them manually. We created two users and two listings, each user connected to one of the listings.

## Constraint
The email should be using the format of [A-z]\*@[A-z]\*\\.[A-z]*. Therefore we added a regex validation to the user constructor.
![validator image](validator.png "Email validation")
